# Shared Preference

**Get user input and save**
```java 
SharedPreferences sharedPreferences = getSharedPreferences("Shared Preferences", Context.MODE_PRIVATE);
SharedPreferences.Editor editor = sharedPreferences.edit();

// getting user input 
String message = editText.getText().toString().trim();

// checking null input
if (!message.equals("")) {
    editor.putString("message", message); // put-key
    editor.apply(); // commit
    Toast.makeText(getApplicationContext(), "Data has been saved successfully", Toast.LENGTH_SHORT).show(); // confirm 
} else  {
    Toast.makeText(getApplicationContext(), "Data is empty", Toast.LENGTH_SHORT).show(); // confirm 
}
```
**Display Data on TextView**
```java
String displayMessage = sharedPreferences.getString("message", ""); // get string from saved data
textView.setText(displayMessage);
```

**Source:** <br>
[Android - Shared Preferences (tutorialspoint.com)](https://www.tutorialspoint.com/android/android_shared_preferences.htm)


Farhan Sadik
